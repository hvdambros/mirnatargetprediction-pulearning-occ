# miRNA target prediction with PU Learning and One-Class Classification #

### python libraries needed ###
  - python=3.7
  - pandas
  - numpy
  - matplotlib
  - scikit-learn
  - imbalanced-learn